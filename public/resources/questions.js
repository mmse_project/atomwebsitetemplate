var allQuestions = [];
var selectedLanguage = null;
var selectedImage = null;

function getQuestions(language) {
  selectedLanguage = language;
  fetch('http://localhost:8080/questions/' + language)
    .then(response => response.json())
    .then(questions => {
      allQuestions = questions;
      displayQuestions(questions);
    });
}

function getAllQuestions() {
  fetch('http://localhost:8080/questions/')
    .then(response => response.json())
    .then(questions => {
      allQuestions = questions;
      displayQuestions(questions);
    });
}

function displaySubQuestions(subQuestions) {

  if (subQuestions != null) {
    let subQuestionsView = "";
    subQuestionsView = subQuestionsView + "<table class='table' width='100%'>";
    for (let i = 0; i < subQuestions.length; i++) {
      //console.log(getSubQuestions(question))[i];
      subQuestionsView = subQuestionsView + "<tr>";
      subQuestionsView = subQuestionsView + "<td style='width: 600px;'>";
      subQuestionsView = subQuestionsView + subQuestions[i].subquestiontext + "<br/>";

      if (subQuestions[i].image != null) {
        subQuestionsView = subQuestionsView + `<img src="${subQuestions[i].image}"><br/>`;
      }

      subQuestionsView = subQuestionsView + "</td>";
      subQuestionsView = subQuestionsView + "<td style='width: 60px;'>";
      subQuestionsView = subQuestionsView + renderPointsDropdown(subQuestions[i].id, subQuestions[i].answer);
      subQuestionsView = subQuestionsView + "<br/>";
      subQuestionsView = subQuestionsView + "</td>";
      subQuestionsView = subQuestionsView + "</tr>";
    }
    subQuestionsView = subQuestionsView + "</table>";
    return subQuestionsView;
  } else {
    return "";
  }
}

function displayQuestions(questions) {
  let questionList = document.getElementById('questionList');
  questionList.innerHTML = "";

  for (let i = 0; i < questions.length; i++) {
    let questionRow = "";
    questionRow = "<tr>";
    // questionRow = questionRow + "<td>" + questions[i].id + "</td>";
    questionRow = questionRow + "<td>" + questions[i].questiontext + "</td>";
    questionRow = questionRow + "</td></tr>";
    questionRow = questionRow + "<tr><td colspan='2'>";
    // // Alamküsimused
    questionRow = questionRow + displaySubQuestions(questions[i].subQuestions);
    questionRow = questionRow + "</td></tr>";
    questionRow = questionRow + "</tr>";
    questionList.innerHTML += questionRow;
  }

  // näidake nuppu!

  if (selectedLanguage == 'estonian') {
    document.getElementById("calculateButtonDivEst").style.display = "block";
    document.getElementById("calculateButtonDivRus").style.display = "none";
  } else {
    document.getElementById("calculateButtonDivEst").style.display = "none";
    document.getElementById("calculateButtonDivRus").style.display = "block";
  }

}

function calculatePoints() {
  let sum = 0;
  for (i = 0; i < allQuestions.length; i++) {
    if (allQuestions[i].subQuestions != null) {
      for (j = 0; j < allQuestions[i].subQuestions.length; j++) {
        let currentSubQuestionId = allQuestions[i].subQuestions[j].id;
        if (
          parseInt(document.getElementById(currentSubQuestionId).value) <= parseInt(allQuestions[i].subQuestions[j].answer) &&
          parseInt(document.getElementById(currentSubQuestionId).value) >= 0
        ) {
          sum += parseInt(document.getElementById(currentSubQuestionId).value);
        }
      }
    }
  }
  displayAlert(sum);
}


function renderPointsDropdown(id, maxPoints) {
  let allPoints = Array.from(new Array(parseInt(maxPoints) + 1), (val, index) => index);
  let dropDownTemplate = `
    <select class="form-control" id="${id}" style="width: 60px;">
    <option value="">---</option>
    ${allPoints.map(point => `<option value="${point}">${point}</option>`)}
    </select>
  `;
  return dropDownTemplate;
}

function displayAlert(sum) {
  let resultText = "";
  if (selectedLanguage == 'estonian') {
    if (sum >= 25) {
      resultText = sum + '<br/>Dementsusilminguid ei ole ja testi kliiniline olulisus on küsitav.<br/>Edasine testimine võib olla kasulik, kui esinevad kliinilised tunnused.<br/>Toimetulek on normaalne.<br/>Ainult testi tulemusest dementsuse diagnoosimiseks ei piisa. Samuti ei anna test informatsiooni dementsuse põhjuse kohta. Selleks on vaja lisauuringuid.';
    } else if (sum >= 20) {
      resultText = sum + '<br/>Kerge dementsus.<br/>Edasine testimine võib olla kasulik, et hinnata defitsiidi ulatust ja väljendumist.<br/>Toimetulekus oluline tagasilangus, võib vajada järelevalvet ja abi.<br/>Ainult testi tulemusest dementsuse diagnoosimiseks ei piisa. Samuti ei anna test informatsiooni dementsuse põhjuse kohta. Selleks on vaja lisauuringuid.';
    } else if (sum >= 10) {
      resultText = sum + '<br/>Mõõdukas dementsus.<br/>Edasine testimine võib olla kasulik.<br/>Toimetulek on selgelt vähenenud, võib vajada ööpäevaringset järelevalvet.<br/>Ainult testi tulemusest dementsuse diagnoosimiseks ei piisa. Samuti ei anna test informatsiooni dementsuse põhjuse kohta. Selleks on vaja lisauuringuid.';
    } else {
      resultText = sum + '<br/>Raske dementsus.<br/>Pole tõenäoliselt võimalik testida.<br/>Raske dementsus, mis vajab igakülgset abi ja ööpäevaringset järelevalvet.<br/>Ainult testi tulemusest dementsuse diagnoosimiseks ei piisa. Samuti ei anna test informatsiooni dementsuse põhjuse kohta. Selleks on vaja lisauuringuid.';
    }
  } else {
    if (sum >= 25) {
      resultText = sum + '<br/>Нет нарушений когнитивных функций.';
    } else if (sum >= 20) {
      resultText = sum + '<br/>Деменция легкой степени. <br/>Наличии выраженных когнитивных нарушений. В этом случае следует выяснить, оказывают ли эти расстройства значимое негативное влияние на повседневную жизнь.';
    } else if (sum >= 10) {
      resultText = sum + '<br/>Деменция умеренной степени.<br/>Наличии выраженных когнитивных нарушений. В этом случае следует выяснить, оказывают ли эти расстройства значимое негативное влияние на повседневную жизнь.';
    } else {
      resultText = sum + '<br/>Тяжелая деменция.';
    }
  }

  document.getElementById("punktiskoor").innerHTML = resultText;
  document.getElementById("punktiskoor").style.display = "block";
}